export const lengths = {
  betweenSections: 50,
  betweenSiblings: 10
}

export const keys = {
  accessToken: 'Token:access',
  refreshToekn: 'Token:refresh'
}

export const settings = {
  storageRetryLimit: 5
}
