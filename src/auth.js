// TODO Is AsyncStorage a safe enough way to store tokens?
import { AsyncStorage } from 'react-native'
import { keys, settings } from './constants'

export const saveTokens = async (tokens, numRetries = 0) => {
  try {
    await AsyncStorage.setItem(keys.accessToken, tokens.access)
    await AsyncStorage.setItem(keys.refreshToekn, tokens.refresh)
  } catch (error) {
    // TODO Handle errors more carefully
    if (numRetries < settings.storageRetryLimit) {
      saveTokens(tokens, numRetries + 1)
    } else {
      throw error
    }
  }
}

export const getTokens = async (numRetries = 0) => {
  try {
    const accessToken = await AsyncStorage.getItem(keys.accessToken)
    const refreshToken = await AsyncStorage.getItem(keys.refreshToken)

    if (accessToken && refreshToken) {
      return {
        access: accessToken,
        refresh: refreshToken
      }
    } else {
      return null
    }
  } catch (error) {
    // TODO Handle errors more carefully
    if (numRetries < settings.storageRetryLimit) {
      return getTokens(numRetries + 1)
    } else {
      throw error
    }
  }
}
