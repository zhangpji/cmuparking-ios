import React, { Component } from 'react'
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Content,
  Text } from 'native-base'

export class ReportViolation extends Component {
  render () {
    return (
      <Container>
        <Header>
          <Left />
          <Body style={{ flex: 3 }}>
            <Title>
              Report Violation
            </Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Text>Report Violation</Text>
        </Content>
      </Container>
    )
  }
}
