import React, { Component } from 'react'
import {
  Container,
  Content,
  Text } from 'native-base'

export class NewReservation extends Component {
  static navigationOptions = {
    title: 'New Reservation'
  }

  render () {
    return (
      <Container>
        <Content padder>
          <Text>New Reservation</Text>
        </Content>
      </Container>
    )
  }
}
