import React from 'react'
import { Footer, FooterTab, Icon, Button, Text } from 'native-base'
import { createBottomTabNavigator } from 'react-navigation'
import { Reservation } from './reservation'
import { ReportViolation } from './report-violation'
import { Profile } from './profile'

export const Home = createBottomTabNavigator({
  Reservation: Reservation,
  Violation: ReportViolation,
  Profile: Profile
}, {
  navigationOptions: ({ navigation }) => ({
    tabBarComponent: () => {
      const { routeName } = navigation.state
      return (
        <Footer>
          <FooterTab>
            <Button
              vertical
              active={routeName === 'Reservation'}
              onPress={() => { navigation.navigate('Reservation') }}>
              <Icon name='ios-list-box-outline' />
              <Text>Reservations</Text>
            </Button>
            <Button
              vertical
              active={routeName === 'Violation'}
              onPress={() => { navigation.navigate('Violation') }}>
              <Icon name='ios-hand-outline' />
              <Text>Violation</Text>
            </Button>
            <Button
              vertical
              active={routeName === 'Profile'}
              onPress={() => { navigation.navigate('Profile') }}>
              <Icon name='ios-person-outline' />
              <Text>Profile</Text>
            </Button>
          </FooterTab>
        </Footer>
      )
    }
  })
})
