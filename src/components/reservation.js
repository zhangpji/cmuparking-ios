import { createStackNavigator } from 'react-navigation'
import { ReservationList } from './reservation-list'
import { NewReservation } from './new-reservation'

export const Reservation = createStackNavigator({
  List: ReservationList,
  New: NewReservation
}, {
  initialRouteName: 'List',
  cardStyle: {
    backgroundColor: '#fff'
  }
})
