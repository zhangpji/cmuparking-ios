import React, { Component } from 'react'
import { View } from 'react-native'
import {
  Container,
  Content,
  H1,
  Form,
  Item,
  Input,
  Label,
  Button,
  Text } from 'native-base'
import { createStackNavigator } from 'react-navigation'
import { lengths } from '../constants'

const BlockButton = props => {
  const { style, label, ...rest } = props
  return (
    <Button
      block
      style={{ margin: 10, ...style }}
      {...rest}>
      <Text>{label}</Text>
    </Button>
  )
}

export class SignIn extends Component {
  constructor (props) {
    super(props)
    this.state = {
      email: '',
      password: ''
    }
  }

  static navigationOptions = {
    header: null
  }

  render () {
    return (
      <Container>
        <Content
          padder
          contentContainerStyle={{
            flex: 1,
            justifyContent: 'center'
          }}>
          <View
            style={{ marginBottom: lengths.betweenSections }}>
            <H1 style={{ textAlign: 'center' }}>CMU Parking</H1>
          </View>
          <Form>
            <Item floatingLabel>
              <Label>Email</Label>
              <Input
                value={this.state.email}
                onChangeText={val => { this.setState({ email: val }) }}
                autoFocus
                autoCapitalize='none'
                autoCorrect={false}
                keyboardType='email-address' />
            </Item>
            <Item floatingLabel last>
              <Label>Password</Label>
              <Input
                value={this.state.password}
                onChangeText={val => { this.setState({ password: val }) }}
                secureTextEntry />
            </Item>
          </Form>
          <View
            style={{ marginTop: lengths.betweenSections }}>
            <BlockButton label='Sign In' />
            <BlockButton
              bordered
              onPress={() => { this.props.navigation.navigate('SignUp') }}
              label='Sign Up' />
          </View>
        </Content>
      </Container>
    )
  }
}

export class SignUp extends Component {
  constructor (props) {
    super(props)

    this.state = {
      email: '',
      name: '',
      password: '',
      phoneNumber: '',
      plateNumber: ''
    }
  }

  static navigationOptions = {
    title: 'Sign Up'
  }

  render () {
    return (
      <Container>
        <Content padder>
          <Form>
            <Item floatingLabel>
              <Label>Email</Label>
              <Input
                value={this.state.email}
                onChangeText={val => { this.setState({ email: val }) }}
                autoFocus
                autoCapitalize='none'
                autoCorrect={false}
                keyboardType='email-address' />
            </Item>
            <Item floatingLabel>
              <Label>Password</Label>
              <Input
                value={this.state.password}
                onChangeText={val => { this.setState({ password: val }) }}
                secureTextEntry />
            </Item>
            <Item floatingLabel>
              <Label>Name</Label>
              <Input
                value={this.state.name}
                onChangeText={val => { this.setState({ name: val }) }}
                autoCapitalize='none'
                autoCorrect={false} />
            </Item>
            <Item floatingLabel>
              <Label>Phone Number</Label>
              <Input
                value={this.state.phoneNumber}
                onChangeText={val => { this.setState({ phoneNumber: val }) }}
                keyboardType='phone-pad' />
            </Item>
            <Item floatingLabel last>
              <Label>Plate Number</Label>
              <Input
                value={this.state.plateNumber}
                onChangeText={val => { this.setState({ plateNumber: val }) }}
                autoCapitalize='characters' />
            </Item>
          </Form>
          <View style={{ marginTop: lengths.betweenSections }}>
            <BlockButton label='Sign Up' />
          </View>
        </Content>
      </Container>
    )
  }
}

export const Account = createStackNavigator({
  SignIn: SignIn,
  SignUp: SignUp
}, {
  initialRouteName: 'SignIn',
  cardStyle: {
    backgroundColor: '#fff'
  }
})
