import * as Expo from 'expo'
import React, { Component } from 'react'
import { Root } from './root'

export class App extends Component {
  constructor () {
    super()
    this.state = {
      isReady: false
    }
  }

  async componentDidMount () {
    await Expo.Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      Ionicons: require('@expo/vector-icons/fonts/Ionicons.ttf')
    })
    this.setState({ isReady: true })
  }

  render () {
    if (!this.state.isReady) {
      return <Expo.AppLoading />
    } else {
      return <Root />
    }
  }
}
