import React from 'react'
import { Home } from './home'
import { Account } from './account'
import { Provider, Consumer } from '../store'

const App = props => (
  <Consumer>
    {({ user }) => (
      user.email ? <Home /> : <Account />
    )}
  </Consumer>
)

export const Root = props => (
  <Provider>
    <App />
  </Provider>
)
