import React, { Component } from 'react'
import {
  Container,
  Content,
  Header,
  Left,
  Body,
  Right,
  Text,
  Title } from 'native-base'

export class Profile extends Component {
  render () {
    return (
      <Container>
        <Header>
          <Left />
          <Body>
            <Title>Profile</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Text>Profile</Text>
        </Content>
      </Container>
    )
  }
}
