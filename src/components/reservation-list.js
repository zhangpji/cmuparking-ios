import React, { Component } from 'react'
import { View } from 'react-native'
import {
  Container,
  Header,
  Content,
  Text,
  Card,
  CardItem,
  Button,
  Icon,
  Left,
  Body,
  Right,
  Title } from 'native-base'
import { lengths } from '../constants'
import { Consumer } from '../store'

const Entry = props => {
  const { text, style, icon } = props

  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        ...style
      }}>
      <Icon
        name={icon}
        style={{
          width: 24,
          fontSize: 24,
          textAlign: 'center',
          marginRight: lengths.betweenSiblings
        }} />
      <Text>{text}</Text>
    </View>
  )
}

const ReservationItem = props => {
  const { reservation } = props
  return (
    <Card>
      <CardItem header bordered>
        <Text>Starts in 60 minutes</Text>
      </CardItem>
      <CardItem bordered>
        <Body>
          <Entry
            text={`${reservation.start} → ${reservation.end}, June 1`}
            icon='ios-clock-outline' />
          <Entry
            style={{ marginTop: lengths.betweenSiblings }}
            text={`${reservation.location}, Spot ${reservation.spot}`}
            icon='ios-pin-outline' />
        </Body>
      </CardItem>
      <CardItem footer bordered>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-end'
          }}>
          <Button transparent small primary>
            <Text>Check In</Text>
          </Button>
          <Button transparent small danger>
            <Text>Cancel</Text>
          </Button>
        </View>
      </CardItem>
    </Card>
  )
}

const ReservationListRender = props => (
  <Container>
    <Content padder>
      {props.reservations.map(x => (
        <ReservationItem
          reservation={x}
          key={x.id} />
      ))}
    </Content>
  </Container>
)

export class ReservationList extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: (
      <Header>
        <Left />
        <Body>
          <Title>Reservations</Title>
        </Body>
        <Right>
          <Button
            transparent
            onPress={() => { navigation.navigate('New') }}>
            <Icon name='ios-add-circle-outline' />
          </Button>
        </Right>
      </Header>
    )
  })

  render () {
    return (
      <Consumer>
        {({ reservations }) => <ReservationListRender reservations={reservations} />}
      </Consumer>
    )
  }
}
