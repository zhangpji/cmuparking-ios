import React, { Component } from 'react'

const defaultState = {
  user: {
    email: '',
    name: '',
    plateNumber: '',
    phoneNumber: ''
  },
  reservations: []
}

const store = React.createContext(defaultState)

export class Provider extends Component {
  constructor (props) {
    super(props)
    this.state = defaultState
  }

  render () {
    return (
      <store.Provider value={this.state}>
        {this.props.children}
      </store.Provider>
    )
  }
}

export const Consumer = store.Consumer
